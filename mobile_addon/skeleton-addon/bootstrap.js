const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://gre/modules/Services.jsm");

function isNativeUI() {
  return (Services.appinfo.ID == "{aa3c5121-dab2-40e2-81ca-7ea25febc110}");
}

function showToast(aWindow) {
  aWindow.NativeWindow.toast.show("Showing you a toast", "short");
}

function showToastMsg(aWindow, aMessage) {
  aWindow.NativeWindow.toast.show(aMessage, "short");
}

function connectToServer(aWindow) {
	var http = new XMLHttpRequest();
	//http.open("GET", "costarica.cc.gatech.edu:8080/dump/info?TextField=http://www.google.com&Action=Submit",true);
	//http.send();

/*
  var socket = io.connect('ws://192.168.123.117:8887/');
  socket.on('connect', function () {
    socket.send('hi');

    socket.on('message', function ("hi") {
      // my msg
    });
  });
*/
/*
    var ws = new MozWebSocket("http://192.168.123.117:8887");
    ws.onopen = function() {
		showToastMsg(aWindow, 'Sending');
        ws.send('http://www.mobile.com/');
    }   
    ws.onmessage = function (evt) { 
        if (evt.data.toLowerCase() == "benign") {
			showToastMsg(aWindow, 'benign');
        }   
        if (evt.data.toLowerCase() == "malicious") {
			showToastMsg(aWindow, 'malicious');
        }   
    };  
    ws.onclose = function() { // websocket is closed. 
    };  
*/
}

function showDoorhanger(aWindow) {
  buttons = [
    {
      label: "Button 1",
      callback: function() {
        aWindow.NativeWindow.toast.show("Button 1 was tapped", "short");
      }
    } , {
      label: "Button 2",
      callback: function() {
        aWindow.NativeWindow.toast.show("Button 2 was tapped", "short");
      }
    }];

  aWindow.NativeWindow.doorhanger.show("Showing a doorhanger with two button choices.", "doorhanger-test", buttons);
}

function copyLink(aWindow, aTarget) {
  let url = aWindow.NativeWindow.contextmenus._getLinkURL(aTarget);
  aWindow.NativeWindow.toast.show("Todo: copy > " + url, "short");
}

var gToastMenuId = null;
var gDoorhangerMenuId = null;
var gContextMenuId = null;
var gConnectServerMenuId = null;

function loadIntoWindow(window) {
  if (!window)
    return;

  if (isNativeUI()) {
    gToastMenuId = window.NativeWindow.menu.add("Show Toast", null, function() { 
		//showToast(window); 
		showToastMsg(window, "hi");
	});
    gDoorhangerMenuId = window.NativeWindow.menu.add("Show Doorhanger", null, function() { 
		showDoorhanger(window); 
	});
    gContextMenuId = window.NativeWindow.contextmenus.add("Copy Link", window.NativeWindow.contextmenus.linkOpenableContext, function(aTarget) { 
		copyLink(window, aTarget); 
	});
    gConnectServerMenuId = window.NativeWindow.menu.add("Connect to Server", null, function() { 
		connectToServer(window);
		//var ua = element.setAttribute("userAgent", window.navigator.userAgent);
		//showToastMsg(window, 'ua');
    	/*var ws = new WebSocket("ws://localhost:8887/");
    	ws.onopen = function() {
			showToastsMsg(window, "SENDING");
    	    ws.send("http://www.slashdot.org);
   		}   
    	ws.onmessage = function (evt) { 
     		if (evt.data.toLowerCase() == "benign") {
				showToastsMsg(window, "Recieved: BENIGN");
        	}   
        	if (evt.data.toLowerCase() == "malicious") {
				showToastsMsg(window, "Recieved: MALICIOUS");
        	}   
    	};  
    	ws.onclose = function() { // websocket is closed. 
    	};*/
  
	});
  }
}

function unloadFromWindow(window) {
  if (!window)
    return;
	window.NativeWindow.menu.remove(menuId);
  if (isNativeUI()) {
    window.NativeWindow.menu.remove(gToastMenuId);
    window.NativeWindow.menu.remove(gDoorhangerMenuId);
    window.NativeWindow.menu.remove(gConnectServerMenuId);
    window.NativeWindow.contextmenus.remove(gContextMenuId);
  }
}


/**
 * bootstrap.js API
 */
var windowListener = {
  onOpenWindow: function(aWindow) {
    // Wait for the window to finish loading
    let domWindow = aWindow.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowInternal || Ci.nsIDOMWindow);
    domWindow.addEventListener("load", function() {
      domWindow.removeEventListener("load", arguments.callee, false);
      loadIntoWindow(domWindow);
    }, false);
  },
  
  onCloseWindow: function(aWindow) {
  },
  
  onWindowTitleChange: function(aWindow, aTitle) {
  }
};

function startup(aData, aReason) {
  // Load into any existing windows
  let windows = Services.wm.getEnumerator("navigator:browser");
  while (windows.hasMoreElements()) {
    let domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
    loadIntoWindow(domWindow);
  }

  // Load into any new windows
  Services.wm.addListener(windowListener);

	

}

function onWebPageLoad(event) {
  if (event.originalTarget instanceof HTMLDocument) {
    let htmldoc = event.originalTarget;
	event.NativeWindow.toast.show("Showing you a toast", "short");	
  }
}

function shutdown(aData, aReason) {
  // When the application is shutting down we normally don't have to clean
  // up any UI changes made
  if (aReason == APP_SHUTDOWN)
    return;

	let wm = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindoMediator);

  // Stop listening for new windows
  Services.wm.removeListener(windowListener);

  // Unload from any existing windows
  let windows = Services.wm.getEnumerator("navigator:browser");
  while (windows.hasMoreElements()) {
    let domWindow = windows.getNext().QueryInterface(Ci.nsIDOMWindow);
    unloadFromWindow(domWindow);
  }
}

function install(aData, aReason) {
}

function uninstall(aData, aReason) {
}
