function MobiBarSearch(event){
	var query = document.getElementById("MobiBarQuery").value;

	var server = 'localhost';
	var port = 12345;
	var data = query;

  	var ws = new WebSocket("ws://localhost:8887/");
  	ws.onopen = function() {
   		//alert('Sending' + data);
   		ws.send(data);
 	}
 	ws.onmessage = function (evt) { 
		//alert('Received: ' + evt.data);
		if (evt.data.toLowerCase() == "benign") {
         	//alert('Received: ' + evt.data);
			window._content.document.location  = encodeURI(query);
		}
		if (evt.data.toLowerCase() == "malicious") {
			//alert('Malicious: ' + evt.data);
			let prompts = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
			//if (prompts.confirm(window, "Warning: Site seems suspicious, could be malicious!", "Would you like to continue?")) {
			var check = {value: false};
			var result = prompts.confirmCheck(null, "kAYO: This site seems suspicious and possibly malicious", "Would you still like to continue?", "Ignore this warning", check);
			if (result) {
				window._content.document.location = encodeURI(query);
			}
		}
 	};
   	ws.onclose = function() { // websocket is closed. 
    };

     //alert('readystate: ' + ws.readyState + " for " + ws.URL);

}
 
